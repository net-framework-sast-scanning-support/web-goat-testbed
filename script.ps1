Remove-Item -LiteralPath "security-code-scanner" -Force -Recurse -ErrorAction SilentlyContinue
New-Item -Name "security-code-scanner" -ItemType "directory"
Invoke-WebRequest -Uri https://github.com/security-code-scan/security-code-scan/releases/download/5.6.0/security-scan4x.zip -OutFile "security-code-scanner/scanner.zip"
Expand-Archive ./security-code-scanner/scanner.zip -DestinationPath ./security-code-scanner
./security-code-scanner/security-scan.exe (Get-ChildItem -Path *.sln -Force -File | Select-Object -First 1).Name --excl-proj=**.*Test** --export=scan-result.json

Remove-Item -LiteralPath "net-framework-mapper" -Force -Recurse -ErrorAction SilentlyContinue
New-Item -Name "net-framework-mapper" -ItemType "directory"
Invoke-WebRequest -Uri https://gitlab.com/net-framework-sast-scanning-support/scan-mapper/-/jobs/2052902500/artifacts/download -OutFile "net-framework-mapper/artifacts.zip"
Expand-Archive ./net-framework-mapper/artifacts.zip -DestinationPath ./net-framework-mapper
./net-framework-mapper/NetFrameworkSecurityScanParser/cli ./scan-result.json
